// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    val kotlin_version by extra("1.3.72")
    repositories {
        google()
        jcenter()
        maven("https://jitpack.io")
        maven("https://plugins.gradle.org/m2/")
    }
    dependencies {
        classpath("com.android.tools.build:gradle:3.6.3")
        classpath(kotlin("gradle-plugin", version = "1.3.70"))
        classpath("com.google.gms:google-services:4.3.3")
        classpath("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:2.8")
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:2.2.2")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:9.2.1")
        classpath ("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.8.0")
        classpath ("com.google.firebase:firebase-appdistribution-gradle:1.4.1")
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven("https://jitpack.io")
    }
}

tasks.register<Delete>("clean") {
    delete (rootProject.buildDir)
}