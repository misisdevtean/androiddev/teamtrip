package com.misisdevteam.routy.view.registerBlock.login.ui

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginFragmentTest {

    @Test
    fun getBindingVariable() {
    }

    @Test
    fun getLayoutId() {
    }

    @Test
    fun getViewModel() {
    }

    @Test
    fun onCreate() {
    }

    @Test
    fun forgotPassword() {
    }

    @Test
    fun createAccount() {
    }

    @Test
    fun loginCompleted() {
    }

    @Test
    fun authWithGoogle() {
    }

    @Test
    fun onActivityResult() {
    }
}
