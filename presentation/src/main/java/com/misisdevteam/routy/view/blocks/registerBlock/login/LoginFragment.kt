
package com.misisdevteam.routy.view.blocks.registerBlock.login

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.viewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.LoginFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.LoginViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class LoginFragment :
    BaseFragment<LoginFragmentBinding, LoginViewModel>(R.layout.login_fragment) {

    private val viewModel: LoginViewModel by viewModel()
    override fun getBindingVariable() = BR.viewModel

    override fun getFragmentViewModel(): LoginViewModel = viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0) {
            viewModel.handleGoogleSignInResult()
        }
    }
}
