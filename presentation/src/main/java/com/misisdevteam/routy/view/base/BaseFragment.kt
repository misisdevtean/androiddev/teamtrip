package com.misisdevteam.routy.view.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.misisdevteam.routy.R
import com.misisdevteam.routy.TeamTripApp
import com.misisdevteam.routy.data.ScreenState
import com.misisdevteam.routy.helpers.SnackbarHelper
import com.misisdevteam.routy.navigation.NavigationCommand
import com.misisdevteam.routy.view.base.uiLifecycleScope.UiLifecycleScope
import com.misisdevteam.routy.view.mainContainer.MainContainerActivity
import timber.log.Timber

abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel>(private val layoutId: Int) :
        Fragment(layoutId) {

    protected lateinit var dataBinding: T
    private var mViewModel: V? = null
    private lateinit var rootActivity: BaseActivity<*, *>
    private var mRootView: View? = null
    private val uiScope = UiLifecycleScope()

    abstract fun getBindingVariable(): Int

    abstract fun getFragmentViewModel(): V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = getFragmentViewModel()
        lifecycle.addObserver(mViewModel as V)
        lifecycle.addObserver(uiScope)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataBinding.apply {
            setVariable(getBindingVariable(), mViewModel)
            lifecycleOwner = this@BaseFragment
            /*if (this@BaseFragment.javaClass.superclass != BaseFragmentWithAdditionalDataBindingVariables::class.java)
                executePendingBindings()*/
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeNavCommands(mViewModel)
        observeCurrentState(mViewModel)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        dataBinding.let { mRootView = it.root }
        return mRootView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        rootActivity = context as BaseActivity<*, *>
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(mViewModel as V)
    }

    protected fun observeNavCommands(mViewModel : BaseViewModel?) {
        mViewModel?.navCommands?.observe(
                this as LifecycleOwner,
                Observer { command ->
                    when (command) {
                        is NavigationCommand.To -> findNavController().navigate(command.directions)
                        is NavigationCommand.Back -> findNavController().popBackStack()
                        is NavigationCommand.BackTo -> findNavController().popBackStack(command.destinationId, false)
                        is NavigationCommand.ToRoot -> findNavController().popBackStack(com.misisdevteam.routy.R.id.main_fragment, false)
                        is NavigationCommand.DirectTo -> findNavController().navigate(command.direction)
                        is NavigationCommand.WithIntent ->
                            command.expectedResult?.let { startActivityForResult(command.intent, command.expectedResult) }
                                    ?: kotlin.run { startActivity(command.intent) }
                    }
                }
        )
    }

    private fun observeCurrentState(mViewModel : BaseViewModel?) {
        mViewModel?.currentState?.observe(
                this as LifecycleOwner,
                Observer { state ->
                    when (state) {
                        is ScreenState.Loading -> showLoading()
                        else -> {
                            hideLoading()
                            when (state) {
                                is ScreenState.Error -> showError(state.e)
                            }
                        }
                    }
                }
        )
    }

    private fun showLoading() {
        // TODO("implement this")
        Toast.makeText(requireContext(), "start loading", Toast.LENGTH_SHORT).show()
        Timber.i("Start loading")
    }

    private fun hideLoading() {
        Toast.makeText(requireContext(), "stop loading", Toast.LENGTH_SHORT).show()
        Timber.i("Stop loading")
    }

    private fun showError(exception: Exception?) {
        exception?.let {
            SnackbarHelper.showSnackBar(
                    Snackbar.make(
                            requireView(),
                            exception.message.toString(), Snackbar.LENGTH_LONG
                    ),
                    this.requireContext(),
                    isError = true
            )
        }
    }
}
