package com.misisdevteam.routy.view.blocks.registerBlock.confirm_email

import androidx.fragment.app.viewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.CreateAccountFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.ConfirmEmailViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ConfirmEmailFragment : BaseFragment<CreateAccountFragmentBinding, ConfirmEmailViewModel>(R.layout.confirm_email_fragment) {
    private val viewModel: ConfirmEmailViewModel by viewModel()

    override fun getBindingVariable() = BR.viewModel

    override fun getFragmentViewModel(): ConfirmEmailViewModel = viewModel
}
