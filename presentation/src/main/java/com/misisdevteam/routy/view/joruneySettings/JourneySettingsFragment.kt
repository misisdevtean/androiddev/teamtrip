package com.misisdevteam.routy.view.joruneySettings

import androidx.fragment.app.viewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.JourneySettingsFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.JourneySettingsViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class JourneySettingsFragment : BaseFragment<JourneySettingsFragmentBinding, JourneySettingsViewModel>(R.layout.journey_settings_fragment){

    private val viewModel: JourneySettingsViewModel by viewModel()

    override fun getBindingVariable() = BR.viewModel

    override fun getFragmentViewModel(): JourneySettingsViewModel = viewModel

}