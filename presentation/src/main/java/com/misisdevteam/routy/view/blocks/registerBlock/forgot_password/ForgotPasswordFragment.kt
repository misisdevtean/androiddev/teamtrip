package com.misisdevteam.routy.view.blocks.registerBlock.forgot_password

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.ForgotPasswordFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.ForgotPasswordViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ForgotPasswordFragment :
    BaseFragment<ForgotPasswordFragmentBinding, ForgotPasswordViewModel>(R.layout.forgot_password_fragment) {

    private val viewModel: ForgotPasswordViewModel by viewModel()

    override fun getBindingVariable() = BR.viewModel

    override fun getFragmentViewModel(): ForgotPasswordViewModel = viewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}
