package com.misisdevteam.routy.view.base

import android.content.Intent
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.misisdevteam.routy.data.Resource
import com.misisdevteam.routy.data.ScreenState
import com.misisdevteam.routy.data.enums.EventType
import com.misisdevteam.routy.navigation.NavigationCommand
import com.misisdevteam.routy.utils.SingleLiveEvent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber

abstract class BaseViewModel : ViewModel(), LifecycleObserver {

    val navCommands = SingleLiveEvent<NavigationCommand>()

    val currentState = SingleLiveEvent<ScreenState>()
    val eventHelper = EventHelper()

    protected val navigator = Navigator()
    protected val baseCoroutineExceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        setState(ScreenState.Error(Exception(throwable)))
        Timber.e(throwable.localizedMessage)
    }

    private val screenEvents = ArrayList<Event>()


    private fun setState(state: ScreenState) {
        currentState.postValue(state)
    }

    fun <T : Resource<*>> LiveData<T>.handleResource() {

    }

    fun <T : Resource<*>> Flow<T>.handleResource(onSuccess: (() -> Unit)? = null,
                                                 onFailure: (() -> Unit)? = null) {
        onEach {
            when (it) {
                is Resource.Loading -> setState(ScreenState.Loading)
                is Resource.Error -> {
                    setState(ScreenState.Error(it.exception))
                    onFailure?.invoke()
                }
                is Resource.Success<*> -> onSuccess?.invoke()
            }
        }.launchIn(viewModelScope)
    }

    protected fun registerScreenEvents(vararg events: Event) {
        screenEvents.addAll(events)
    }

    /**
     * handle screen events as Click, Scroll etc
     */
    fun handleEvent(id: Int, eventType: EventType) {
        screenEvents.forEach {
            if (it.id == id && it.eventType == eventType) {
                viewModelScope.launch(baseCoroutineExceptionHandler) {
                    it.func.invoke()
                }
                return
            }
        }
    }

    open inner class Navigator {
        fun navigate(directions: NavDirections) {
            navCommands.postValue(NavigationCommand.To(directions))
        }

        fun navigate(direction: Int) {
            navCommands.postValue(NavigationCommand.DirectTo(direction))
        }

        fun goBack(dest: Int = 0) = if (dest != 0) navCommands.postValue(NavigationCommand.BackTo(dest))
        else navCommands.postValue(NavigationCommand.Back)

        fun toRoot() = navCommands.postValue(NavigationCommand.ToRoot)

        fun startIntent(intent: Intent, expectedResult: Int? = null) = navCommands.postValue(NavigationCommand.WithIntent(intent, expectedResult))
    }

    open inner class EventHelper {
        fun handleOnClickEvent(id: Int) {
            handleEvent(id, EventType.CLICK)
        }

        fun handleOnCheckedChangeEvent(id: Int, checked: Boolean) {
            if (checked)
                handleEvent(id, EventType.CHECKBOX_CHECKED)
            else
                handleEvent(id, EventType.CHECKBOX_UNCHECKED)
        }

    }

    protected data class Event(var id: Int, var eventType: EventType, var func: () -> Unit)
}
