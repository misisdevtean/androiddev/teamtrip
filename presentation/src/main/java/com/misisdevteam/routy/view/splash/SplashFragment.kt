package com.misisdevteam.routy.view.splash

import androidx.fragment.app.viewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.SplashBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.SplashViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class SplashFragment : BaseFragment<SplashBinding, SplashViewModel>(R.layout.splash) {

    private val viewModel: SplashViewModel by viewModel()

    override fun onStart() {
        super.onStart()
        viewModel.toMainFragment()
    }

    override fun getBindingVariable() = BR.viewModel

    override fun getFragmentViewModel() = viewModel
}
