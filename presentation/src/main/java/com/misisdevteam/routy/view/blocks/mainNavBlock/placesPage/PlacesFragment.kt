package com.misisdevteam.routy.view.blocks.mainNavBlock.placesPage

import androidx.fragment.app.viewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.PlacesFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.PlacesViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class PlacesFragment :
    BaseFragment<PlacesFragmentBinding, PlacesViewModel>(R.layout.places_fragment) {

    private val viewModel: PlacesViewModel by viewModel()
    override fun getBindingVariable(): Int = BR.viewModel
    override fun getFragmentViewModel(): PlacesViewModel = viewModel
}
