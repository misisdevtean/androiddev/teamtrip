package com.misisdevteam.routy.view.createJourney

import androidx.lifecycle.ViewModel
import androidx.navigation.navGraphViewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.SubmitJourneyFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragmentWithAdditionalDataBindingVariables
import com.misisdevteam.routy.viewModels.SubmitJourneyViewModel
import com.misisdevteam.routy.viewModels.sharedViewModels.JourneySharedContainer
import org.koin.android.viewmodel.ext.android.viewModel

class SubmitJourneyFragment : BaseFragmentWithAdditionalDataBindingVariables<SubmitJourneyFragmentBinding, SubmitJourneyViewModel>(R.layout.submit_journey_fragment) {

    private val viewModel: SubmitJourneyViewModel by viewModel()
    private val journey : JourneySharedContainer by navGraphViewModels(R.id.create_journey_graph)
    override fun getBindingVariable() = BR.viewModel

    override fun getFragmentViewModel() = viewModel
    override fun getBindingVariables(): ArrayList<Pair<Int,ViewModel>> =
            arrayListOf(BR.journey to journey)
}