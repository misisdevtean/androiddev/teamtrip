package com.misisdevteam.routy.view.createJourney

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.navigation.navGraphViewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.CreateJourneyFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragmentWithAdditionalDataBindingVariables
import com.misisdevteam.routy.viewModels.CreateJourneyViewModel
import com.misisdevteam.routy.viewModels.sharedViewModels.JourneySharedContainer
import org.koin.android.viewmodel.ext.android.viewModel

class CreateJourneyFragment :
        BaseFragmentWithAdditionalDataBindingVariables<CreateJourneyFragmentBinding, CreateJourneyViewModel>
        (R.layout.create_journey_fragment) {
    private val viewModel: CreateJourneyViewModel by viewModel()
    private val journey: JourneySharedContainer by navGraphViewModels(R.id.create_journey_graph)
    override fun getBindingVariable() = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.observeJourney(journey)
    }

    override fun getFragmentViewModel(): CreateJourneyViewModel = viewModel
    override fun getBindingVariables(): ArrayList<Pair<Int,ViewModel>> =
            arrayListOf(BR.journey to journey)
}
