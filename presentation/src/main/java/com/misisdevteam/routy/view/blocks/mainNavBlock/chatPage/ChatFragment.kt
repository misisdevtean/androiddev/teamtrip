package com.misisdevteam.routy.view.blocks.mainNavBlock.chatPage

import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.ChatFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.ChatViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ChatFragment :
    BaseFragment<ChatFragmentBinding, ChatViewModel>(R.layout.chat_fragment) {

    private val viewModel: ChatViewModel by viewModel()
    override fun getBindingVariable(): Int = BR.viewModel
    override fun getFragmentViewModel(): ChatViewModel = viewModel
}
