package com.misisdevteam.routy.view.createJourney

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.navGraphViewModels
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.CalendarFragmentLayoutBinding
import com.misisdevteam.routy.utils.extensions.*
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.CalendarViewModel
import com.misisdevteam.routy.viewModels.sharedViewModels.JourneySharedContainer
import kotlinx.android.synthetic.main.calendar_day.view.*
import kotlinx.android.synthetic.main.calendar_fragment_layout.*
import kotlinx.android.synthetic.main.calendar_header.view.*
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import org.threeten.bp.format.DateTimeFormatter
import org.koin.android.viewmodel.ext.android.viewModel

class CalendarFragment : BaseFragment<CalendarFragmentLayoutBinding, CalendarViewModel>(R.layout.calendar_fragment_layout) {
    private val NO_MARGIN = 0
    private val STANDART_MARGIN = 4

    private val viewModel: CalendarViewModel by viewModel()


    private val dates: JourneySharedContainer by navGraphViewModels(R.id.create_journey_graph)

    private val today = LocalDate.now()

    private var startDate: LocalDate? = null
        set(value) {
            dates.startDate.value = value
            field = value
            updateHeader()
        }

    private var endDate: LocalDate? = null
        set(value) {
            dates.endDate.value = value
            field = value
            updateHeader()
        }

    private fun updateHeader() {
        if (startDate != null && endDate != null)
            dataBinding.dates.text = "$startDate  -  $endDate"
        else if (startDate != null && endDate == null)
            dataBinding.dates.text = "$startDate"
        else if (startDate == null && endDate == null)
            dataBinding.dates.text = ""
    }

    private val headerDateFormatter = DateTimeFormatter.ofPattern("EEE'\n'd MMM")

    private val startBackground: GradientDrawable by lazy {
        requireContext().getDrawable(R.drawable.continuous_selected_bg_start) as GradientDrawable
    }

    private val endBackground: GradientDrawable by lazy {
        requireContext().getDrawable(R.drawable.continuous_selected_bg_end) as GradientDrawable
    }

    private val middleBackground: GradientDrawable by lazy {
        requireContext().getDrawable(R.drawable.continuous_selected_bg_middle) as GradientDrawable
    }

    private val singleChoiceBackground: GradientDrawable by lazy {
        requireContext().getDrawable(R.drawable.single_selected_bg) as GradientDrawable
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        ok.setOnClickListener {
            if (startDate != null && endDate != null)
                viewModel.goBack()
        }
        cancel.setOnClickListener {
            startDate = null
            endDate = null
            viewModel.goBack()
        }
        // We set the radius of the continuous selection background drawable dynamically
        // since the view size is `match parent` hence we cannot determine the appropriate
        // radius value which would equal half of the view's size beforehand.
/*        exFourCalendar.post {
            val radius = 12.toFloat()*//*((exFourCalendar.width / 7) / 2).toFloat()*//*
            startBackground.setCornerRadius(topLeft = radius, bottomLeft = radius)
            endBackground.setCornerRadius(topRight = radius, bottomRight = radius)
        }*/

        // Set the First day of week depending on Locale
        val daysOfWeek = daysOfWeekFromLocale()
/*        legendLayout.children.forEachIndexed { index, view ->
            (view as TextView).apply {
                text = daysOfWeek[index].getDisplayName(TextStyle.SHORT, Locale.ENGLISH)
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
                setTextColorRes(R.color.grey)
            }
        }*/

        val currentMonth = YearMonth.now()
        exFourCalendar.setup(currentMonth, currentMonth.plusMonths(12), daysOfWeek.first())
        exFourCalendar.scrollToMonth(currentMonth)

        class DayViewContainer(view: View) : ViewContainer(view) {
            lateinit var day: CalendarDay // Will be set when this container is bound.
            val textView = view.exFourDayText
            val roundBgView = view.exFourRoundBgView
            val dayContainer = view.dayContainer
            init {
                view.setOnClickListener {
                    if (day.owner == DayOwner.THIS_MONTH && (day.date == today || day.date.isAfter(today))) {
                        val date = day.date
                        if (startDate != null) {
                            if (date < startDate || endDate != null) {
                                startDate = date
                                endDate = null
                            } else if (date != startDate) {
                                endDate = date
                            }
                        } else {
                            startDate = date
                        }
                        exFourCalendar.notifyCalendarChanged()
                        bindSummaryViews()
                    }
                    /*  startDate?.let { date -> startDateLiveData.value = LocalDateTime.of(date, LocalTime.now()).toEpochSecond(ZoneOffset.UTC) }
                      endDate?.let { date -> endDateLiveData.value = LocalDateTime.of(date, LocalTime.now()).toEpochSecond(ZoneOffset.UTC) }
  */
                }
            }
        }
        exFourCalendar.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.textView
                val viewSelector = container.roundBgView
                val dayContainer = container.dayContainer
                textView.text = null
                textView.background = null
                viewSelector.makeInVisible()

                if (day.owner == DayOwner.THIS_MONTH) {
                    if (day.date.dayOfWeek == DayOfWeek.SATURDAY || day.date.dayOfWeek == DayOfWeek.SUNDAY)
                        textView.setTextColorRes(R.color.red)

                    dayContainer.setViewColor(R.color.blurred_black_40)
                    textView.text = day.day.toString()
                    if (day.date.isBefore(today)) {
                        textView.setTextColorRes(R.color.grey_50)
                    } else {
                        when {
                            startDate == day.date && endDate == null -> {
                                textView.setTextColorRes(R.color.grey_white)
                                viewSelector.apply {
                                    makeVisible()
                                    dayContainer.setViewColor(R.color.orange)
                                    setMargins(STANDART_MARGIN, STANDART_MARGIN,
                                            STANDART_MARGIN, STANDART_MARGIN)
                                    background = singleChoiceBackground
                                }
                                dayContainer.setViewColor(R.color.orange)
                            }
                            day.date == startDate -> {
                                textView.setTextColorRes(R.color.grey_white)

                                viewSelector.apply {
                                    makeVisible()
                                    dayContainer.setViewColor(R.color.orange)
                                    setMargins(top = STANDART_MARGIN, bottom = STANDART_MARGIN,
                                            left = STANDART_MARGIN, right = NO_MARGIN)
                                    background = startBackground
                                    //textView.background = startBackground
                                }
                                dayContainer.setViewColor(R.color.orange)
                            }
                            startDate != null && endDate != null && (day.date > startDate && day.date < endDate) -> {
                                textView.setTextColorRes(R.color.grey_white)
                                viewSelector.apply {
                                    makeVisible()
                                    dayContainer.setViewColor(R.color.orange)
                                    setMargins(right = NO_MARGIN, left = NO_MARGIN,
                                            top = STANDART_MARGIN, bottom = STANDART_MARGIN)
                                    viewSelector.background = middleBackground
                                    //textView.setBackgroundResource(R.drawable.continuous_selected_bg_middle)
                                }
                                dayContainer.setViewColor(R.color.orange)
                            }
                            day.date == endDate -> {
                                textView.setTextColorRes(R.color.grey_white)
                                viewSelector.apply {
                                    makeVisible()
                                    dayContainer.setViewColor(R.color.orange)
                                    setMargins(top = STANDART_MARGIN, bottom = STANDART_MARGIN,
                                            left = NO_MARGIN, right = STANDART_MARGIN)
                                    background = endBackground
                                    //textView.background = endBackground
                                }
                                dayContainer.setViewColor(R.color.orange)
                            }
                            /*day.date == today -> {
                                textView.setTextColorRes(R.color.grey)
                                roundBgView.makeVisible()
                                roundBgView.setBackgroundResource(R.drawable.today_bg)
                            }*/
                            //else -> textView.setTextColorRes(R.color.grey)
                        }
                    }
                } else {
                    // This part is to make the coloured selection background continuous
                    // on the blank in and out dates across various months and also on dates(months)
                    // between the start and end dates if the selection spans across multiple months.
                    textView.setTextColorRes(R.color.grey_50)
                    dayContainer.setViewColor(R.color.transparent)
                    val startDate = startDate
                    val endDate = endDate
                    if (startDate != null && endDate != null) {
                        // Mimic selection of inDates that are less than the startDate.
                        // Example: When 26 Feb 2019 is startDate and 5 Mar 2019 is endDate,
                        // this makes the inDates in Mar 2019 for 24 & 25 Feb 2019 look selected.
                        if ((
                                        day.owner == DayOwner.PREVIOUS_MONTH &&
                                                startDate.monthValue == day.date.monthValue &&
                                                endDate.monthValue != day.date.monthValue
                                        ) ||
                                // Mimic selection of outDates that are greater than the endDate.
                                // Example: When 25 Apr 2019 is startDate and 2 May 2019 is endDate,
                                // this makes the outDates in Apr 2019 for 3 & 4 May 2019 look selected.
                                (
                                        day.owner == DayOwner.NEXT_MONTH &&
                                                startDate.monthValue != day.date.monthValue &&
                                                endDate.monthValue == day.date.monthValue
                                        ) ||

                            // Mimic selection of in and out dates of intermediate
                            // months if the selection spans across multiple months.
                            (
                                startDate < day.date && endDate > day.date &&
                                    startDate.monthValue != day.date.monthValue &&
                                    endDate.monthValue != day.date.monthValue
                                )
                        ) {
/*                            roundBgView.apply {
                                makeInVisible()
                                setMargins(right = NO_MARGIN, left = NO_MARGIN,
                                        top = STANDART_MARGIN, bottom = STANDART_MARGIN)
                                roundBgView.background = middleBackground
                            }
                            dayContainer.setViewColor(R.color.transparent)*/
                        }
                    }
                }
            }
        }

        class MonthViewContainer(view: View) : ViewContainer(view) {
            val textView = view.exFourHeaderText
        }
        exFourCalendar.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                val monthTitle = "${month.yearMonth.month.name.toLowerCase().capitalize()} ${month.year}"
                container.textView.text = monthTitle
            }
        }
        bindSummaryViews()
    }

    private fun bindSummaryViews() {
/*
        if (startDate != null) {
            exFourStartDateText.text = headerDateFormatter.format(startDate)
            //exFourStartDateText.setTextColorRes(R.color.grey)
        } else {
            exFourStartDateText.text = getString(R.string.start_date)
            exFourStartDateText.setTextColor(Color.GRAY)
        }
        if (endDate != null) {
            exFourEndDateText.text = headerDateFormatter.format(endDate)
            //exFourEndDateText.setTextColorRes(R.color.grey)
        } else {
            exFourEndDateText.text = getString(R.string.end_date)
            exFourEndDateText.setTextColor(Color.GRAY)
        }
*/
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuItemClear) {
            startDate = null
            endDate = null
            exFourCalendar.notifyCalendarChanged()
            bindSummaryViews()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        val closeIndicator = requireContext().getDrawableCompat(R.drawable.ic_close)?.apply {
            setColorFilter(requireContext().getColorCompat(R.color.grey), PorterDuff.Mode.SRC_ATOP)
        }
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(closeIndicator)
        requireActivity().window.apply {
            // Update statusbar color to match toolbar color.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                statusBarColor = requireContext().getColor(R.color.grey_white)
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                statusBarColor = Color.GRAY
            }
        }
    }

    override fun onStop() {
        super.onStop()
        requireActivity().window.apply {
            // Reset statusbar color.
            statusBarColor = requireContext().getColor(R.color.colorPrimaryDark)
            decorView.systemUiVisibility = 0
        }
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getFragmentViewModel() = viewModel
}
