package com.misisdevteam.routy.view.base

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel

abstract class BaseFragmentWithAdditionalDataBindingVariables<T : ViewDataBinding, V : BaseViewModel>
    (layoutId: Int) : BaseFragment<T,V>(layoutId){

    abstract fun getBindingVariables() : ArrayList<Pair<Int, ViewModel>>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBindingVariables().forEach {
            dataBinding.setVariable(it.first, it.second)
        }
        //dataBinding.executePendingBindings()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getBindingVariables().forEach {
            if (it.second is BaseViewModel){
                observeNavCommands(it.second as BaseViewModel)
            }
        }
    }
}