package com.misisdevteam.routy.view.mainContainer

import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.ActivityMainBinding
import com.misisdevteam.routy.view.base.BaseActivity
import com.misisdevteam.routy.viewModels.MainActivityViewModel

class MainContainerActivity : BaseActivity<ActivityMainBinding, MainActivityViewModel>() {

    private val activityViewModel: MainActivityViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator = Navigation.findNavController(this, R.id.main_container)
        navigator.addOnDestinationChangedListener { controller, destination, arguments ->
            if (destination.id == R.id.loginFragment)
                changeBottomVisiblityState(false)
            else if (destination.id == R.id.main_fragment)
                changeBottomVisiblityState(true)
        }
        getViewDataBinding().mainBottomNav.setupWithNavController(findNavController(R.id.main_container))
    }

    private fun changeBottomVisiblityState(state: Boolean) {
        activityViewModel.isBottomViewEnabled.value = state
    }

    override fun getBindingVariable() = BR.viewModel

    override fun getLayoutId() = R.layout.activity_main

    override fun getViewModel(): MainActivityViewModel {
        return activityViewModel
    }
}
