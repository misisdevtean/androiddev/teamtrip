package com.misisdevteam.routy.view.createJourney

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlin.reflect.KFunction1

class CreateJourneyViewPagerAdapter(
    private val layouts: MutableMap<Int, KFunction1<@ParameterName(name = "view") View, Unit>>
) : RecyclerView.Adapter<CreateJourneyViewPagerAdapter.PagerHolder>() {

    override fun getItemCount() = layouts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PagerHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false), layouts[viewType])
    override fun onBindViewHolder(holder: PagerHolder, position: Int) {
    }

    override fun getItemViewType(position: Int) = layouts.keys.toIntArray()[position]

    inner class PagerHolder(itemView: View, lmbd: KFunction1<View, Unit>?) : RecyclerView.ViewHolder(itemView) {
        init {
            lmbd?.invoke(itemView)
        }
    }
}
