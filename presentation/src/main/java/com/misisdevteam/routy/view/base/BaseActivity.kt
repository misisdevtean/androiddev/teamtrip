package com.misisdevteam.routy.view.base

import android.os.Bundle
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.NavController

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {
    private lateinit var dataBinding: T
    private lateinit var viewModel: V
    lateinit var navigator: NavController
    open fun getViewDataBinding() = dataBinding
    abstract fun getBindingVariable(): Int

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun getViewModel(): V

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility = SYSTEM_UI_FLAG_IMMERSIVE_STICKY or SYSTEM_UI_FLAG_FULLSCREEN
        // window.statusBarColor = resources.getColor(R.color.transparent, theme)
        super.onCreate(savedInstanceState)
        performDataBinding()
    }

    private fun performDataBinding() {
        dataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        this.viewModel = getViewModel()
        dataBinding.setVariable(getBindingVariable(), viewModel)
        dataBinding.executePendingBindings()
        dataBinding.lifecycleOwner = this
    }

    override fun onBackPressed() {
        val fragments = supportFragmentManager.fragments
        for (f in fragments) {
            if (f is OnBackPressedFragment) {
                (f as OnBackPressedFragment).onBackPressed()
            }
        }
        super.onBackPressed()
    }
}
