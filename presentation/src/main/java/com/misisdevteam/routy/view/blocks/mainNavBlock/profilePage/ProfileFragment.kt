package com.misisdevteam.routy.view.blocks.mainNavBlock.profilePage

import androidx.fragment.app.viewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.ProfileFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.ProfileViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ProfileFragment :
    BaseFragment<ProfileFragmentBinding, ProfileViewModel>(R.layout.profile_fragment) {

    private val viewModel: ProfileViewModel by viewModel()
    override fun getBindingVariable(): Int = BR.viewModel
    override fun getFragmentViewModel(): ProfileViewModel = viewModel
}
