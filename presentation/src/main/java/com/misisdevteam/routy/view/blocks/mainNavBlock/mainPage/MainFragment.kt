package com.misisdevteam.routy.view.blocks.mainNavBlock.mainPage

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.MainFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.MainViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class MainFragment :
    BaseFragment<MainFragmentBinding, MainViewModel>(R.layout.main_fragment) {

    private val viewModel: MainViewModel by viewModel()
    override fun getBindingVariable(): Int = BR.viewModel
    override fun getFragmentViewModel(): MainViewModel = viewModel

    override fun onStart() {
        super.onStart()
        viewModel.refreshUser()
        val name = viewModel.curUser.value?.email
        if (name != null) {
            val nameWithComa = "$name, "
            val welcomeToText = resources.getString(R.string.welcome_to)
            val finalText = SpannableString("$nameWithComa$welcomeToText")
            finalText.setSpan(ForegroundColorSpan(resources.getColor(R.color.red, null)), 0, name.length + 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            dataBinding.welcomeTo.text = finalText
        }
    }
}
