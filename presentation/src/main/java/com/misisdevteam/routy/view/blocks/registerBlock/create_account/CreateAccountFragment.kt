package com.misisdevteam.routy.view.blocks.registerBlock.create_account

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.misisdevteam.routy.BR
import com.misisdevteam.routy.R
import com.misisdevteam.routy.databinding.CreateAccountFragmentBinding
import com.misisdevteam.routy.view.base.BaseFragment
import com.misisdevteam.routy.viewModels.CreateAccountViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class CreateAccountFragment : BaseFragment<CreateAccountFragmentBinding, CreateAccountViewModel>(R.layout.create_account_fragment) {
    private val viewModel: CreateAccountViewModel by viewModel()
    override fun getBindingVariable() = BR.viewModel

    override fun getFragmentViewModel(): CreateAccountViewModel = viewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
