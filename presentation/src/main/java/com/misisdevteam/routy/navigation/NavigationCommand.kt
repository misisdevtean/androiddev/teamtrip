package com.misisdevteam.routy.navigation

import android.content.Intent
import androidx.navigation.NavDirections

sealed class NavigationCommand {
    data class To(val directions: NavDirections) : NavigationCommand()
    data class DirectTo(val direction : Int) : NavigationCommand()
    object Back : NavigationCommand()
    data class BackTo(val destinationId: Int) : NavigationCommand()
    object ToRoot : NavigationCommand()
    data class WithIntent(val intent: Intent, val expectedResult: Int?) : NavigationCommand()
}
