package com.misisdevteam.routy.helpers

import android.content.Context
import android.os.Build
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.SnackbarLayout
import com.misisdevteam.routy.R

object SnackbarHelper {
    fun showSnackBar(
        snackbar: Snackbar,
        context: Context,
        withButton: Boolean = false,
        isError: Boolean = false
    ) {
        snackbar.view as SnackbarLayout
        if (isError)
            snackbar.view.background = context.resources.getDrawable(R.drawable.shape_red, context.theme)
        else snackbar.view.background = context.resources.getDrawable(R.drawable.shape_gray_50, context.theme)
        val snackbarText = snackbar.view.findViewById<View>(R.id.snackbar_text) as TextView
        snackbarText.setTextColor(ContextCompat.getColor(context, R.color.grey_white))
        snackbarText.textSize = 14f
        snackbarText.maxLines = 5
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            snackbarText.textAlignment = View.TEXT_ALIGNMENT_CENTER
        } else {
            snackbarText.gravity = Gravity.CENTER_HORIZONTAL
        }
        snackbar.show()
    }
}
