package com.misisdevteam.routy.helpers

import kotlin.math.floor

object IDGenerator {
    fun generateID(): String {
        val chars = "0123456789"
        var id = ""
        for (i in 0..15) {
            id += chars[floor(Math.random() * chars.length).toInt()]
        }
        return id
    }
}
