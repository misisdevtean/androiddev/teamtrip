package com.misisdevteam.routy.helpers

import android.graphics.Bitmap
import android.graphics.BitmapFactory

class ImageHelper {
    companion object {
        fun ByteArray.toBitmap(): Bitmap =
            BitmapFactory.decodeByteArray(this, 0, this.size)
    }
}
