package com.misisdevteam.routy.di.component

import com.misisdevteam.routy.di.module.*

val appComponent = listOf(networkModule, applicationModule, repositoryModule, stringModule, viewModelModule)