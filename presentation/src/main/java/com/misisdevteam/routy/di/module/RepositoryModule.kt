package com.misisdevteam.routy.di.module

import com.misisdevteam.routy.repository.FirebaseAuthRepository
import com.misisdevteam.routy.repository.JourneyRepository
import com.misisdevteam.routy.repository.PrefsRepository
import com.misisdevteam.routy.repository.UserRepository
import org.koin.dsl.module

val repositoryModule = module {

    single {
        return@single PrefsRepository(get())
    }
    single {
        return@single FirebaseAuthRepository(get(), get(), get())
    }
    single {
        return@single JourneyRepository(get())
    }
    single {
        return@single UserRepository(get(), get())
    }
}
