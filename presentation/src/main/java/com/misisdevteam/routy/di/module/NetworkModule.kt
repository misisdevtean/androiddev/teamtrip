package com.misisdevteam.routy.di.module

import com.google.gson.GsonBuilder
import com.misisdevteam.routy.data.TokenController
import com.misisdevteam.routy.utils.Constants
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

val networkModule = module {
    single {
        return@single Retrofit.Builder().client(get())
                .baseUrl(Constants.BACKEND_URL)
                .addConverterFactory(get())
                .build()
    }
    single { TokenController(get()) }
    single<Converter.Factory> {
        return@single GsonConverterFactory
                .create(get())
    }
    single {
        return@single GsonBuilder()
                .setLenient()
                .create()
    }
    single<Interceptor> {
        val tokenController = get<TokenController>()
        class CustomInterceptor : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response = runBlocking {
                var response : Response
                do {
                    response = chain.proceed(chain.request().newBuilder()
                            .addHeader("TT-User-Token", tokenController.key.await())
                            .build())
                    Timber.i("intercept query, inserting auth token = ${tokenController.key}")
                    if (response.code == 401) {
                        Timber.i("token error, refreshing...")
                        tokenController.key.start()
                        response.close()
                        delay(3000)
                    }
                } while (response.code != 200)
                return@runBlocking response
            }
        }
        return@single CustomInterceptor()
    }

    single { return@single Authenticator.NONE }
    single {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        var okHttpClient = OkHttpClient()
        okHttpClient = okHttpClient.newBuilder()
                .authenticator(get())
                .addInterceptor(loggingInterceptor)
                .addInterceptor(get<Interceptor>())
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .build()

        return@single okHttpClient
    }
}