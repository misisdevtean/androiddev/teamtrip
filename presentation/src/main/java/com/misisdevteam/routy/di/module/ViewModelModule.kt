package com.misisdevteam.routy.di.module

import com.misisdevteam.routy.viewModels.*
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { CreateAccountViewModel(get(), get()) }

    viewModel {MainActivityViewModel()}

    viewModel{ LoginViewModel(get(), get())}

    viewModel {ForgotPasswordViewModel(get())}

    viewModel {ConfirmEmailViewModel(get())}

    viewModel {CreateJourneyViewModel(get(), get())}

    viewModel {MainViewModel(get(), get())}

    viewModel {PlacesViewModel()}

    viewModel {ChatViewModel()}

    viewModel {ProfileViewModel()}

    viewModel {SplashViewModel()}

    viewModel {JourneyListViewModel()}

    viewModel {CalendarViewModel()}

    viewModel {SubmitJourneyViewModel()}

    viewModel { JourneySettingsViewModel() }
}


