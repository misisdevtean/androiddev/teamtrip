package com.misisdevteam.routy.viewModels

import com.misisdevteam.routy.view.base.BaseViewModel

class CalendarViewModel : BaseViewModel() {

    fun goBack() = navigator.goBack()
}
