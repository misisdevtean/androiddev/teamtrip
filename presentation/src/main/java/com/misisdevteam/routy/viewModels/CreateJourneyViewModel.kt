package com.misisdevteam.routy.viewModels

import androidx.databinding.ObservableField
import androidx.lifecycle.Observer
import com.misisdevteam.routy.R
import com.misisdevteam.routy.data.enums.EventType
import com.misisdevteam.routy.repository.FirebaseAuthRepository
import com.misisdevteam.routy.repository.JourneyRepository
import com.misisdevteam.routy.utils.editTextUtils.ConfirmButtonErrorCriteria
import com.misisdevteam.routy.utils.editTextUtils.NotNullCriteria
import com.misisdevteam.routy.utils.extensions.addOnPropertyChanged
import com.misisdevteam.routy.view.base.BaseViewModel
import com.misisdevteam.routy.view.createJourney.CreateJourneyFragmentDirections
import com.misisdevteam.routy.viewModels.sharedViewModels.JourneySharedContainer
import org.threeten.bp.LocalDate
import timber.log.Timber

class CreateJourneyViewModel(
        private val firebaseAuthRepository: FirebaseAuthRepository,
        private val journeyRepository: JourneyRepository
) :
        BaseViewModel() {
    init {
        registerScreenEvents(Event(R.id.create_joureny_button, EventType.CLICK, this::createJourney),
                Event(R.id.add_teammate_chip, EventType.CLICK, this::invokeTeammatesFragment),
                Event(R.id.journey_dates, EventType.CLICK, this::invokeCalendarFragment))
    }

    var journeyContainer: JourneySharedContainer? = null

    val journeyName = ObservableField<String>().also {
        it.addOnPropertyChanged { name ->
            journeyContainer?.name?.value = name.get()
        }
    }
    //private val journey = MutableLiveData(Journey())

    private fun invokeCalendarFragment() = navigator.navigate(CreateJourneyFragmentDirections.actionCreateJourneyFragmentToCalendarFragment())
    private fun invokeTeammatesFragment() = navigator.navigate(CreateJourneyFragmentDirections.actionCreateJourneyFragmentToCalendarFragment())

/*    private fun updateJourneyDates(startDate: LocalDate?, endDate: LocalDate?) {
        journeyContainer?.startDate?.value = startDate
        journeyContainer?.endDate?.value = endDate
    }*/

    private fun createJourney() = journeyContainer?.getJourney()?.let {
        journeyRepository.saveJourney(it)
                .handleResource(onSuccess = {
                    navigator.navigate(CreateJourneyFragmentDirections.actionCreateJourneyFragmentToSubmitJourneyFragment())
                })
    } ?: kotlin.run {
        Timber.e("createJourney Error")
    }

    fun observeJourney(journeyContainer: JourneySharedContainer) {
        this.journeyContainer = journeyContainer
        val dateObserver = Observer<LocalDate> {
            if (journeyContainer.startDate.value != null && journeyContainer.endDate.value != null)
                journeyContainer.formattedDates.set("${journeyContainer.startDate.value}  -  ${journeyContainer.endDate.value}")
            else if (journeyContainer.startDate.value != null && journeyContainer.endDate.value == null)
                journeyContainer.formattedDates.set(journeyContainer.startDate.value.toString())
            else if (journeyContainer.startDate.value == null && journeyContainer.endDate.value == null)
                journeyContainer.formattedDates.set("")
        }
        journeyContainer.startDate.observeForever(dateObserver)
        journeyContainer.endDate.observeForever(dateObserver)


    }

    val datesCriteria = NotNullCriteria()
    val nameCriteria = NotNullCriteria()
    val buttonErrorCriteria = ConfirmButtonErrorCriteria(arrayListOf(datesCriteria, nameCriteria))
}
