package com.misisdevteam.routy.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.misisdevteam.routy.R
import com.misisdevteam.routy.data.User
import com.misisdevteam.routy.data.UserParameters
import com.misisdevteam.routy.data.enums.EventType
import com.misisdevteam.routy.repository.FirebaseAuthRepository
import com.misisdevteam.routy.repository.UserRepository
import com.misisdevteam.routy.utils.editTextUtils.ConfirmButtonErrorCriteria
import com.misisdevteam.routy.utils.editTextUtils.EmailCriteria
import com.misisdevteam.routy.utils.editTextUtils.PasswordErrorCriteria
import com.misisdevteam.routy.view.base.BaseViewModel
import com.misisdevteam.routy.view.blocks.registerBlock.login.LoginFragmentDirections
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex

class LoginViewModel(
    private val firebaseAuthRepository: FirebaseAuthRepository,
    private val userRepository: UserRepository
) :
    BaseViewModel() {
    init {
        registerScreenEvents(Event(R.id.google_sign_in, EventType.CLICK, this::loginWithGoogle),
                Event(R.id.create_account_button, EventType.CLICK, this::createAcc),
                Event(R.id.forgot_pass, EventType.CLICK, this::forgotPass),
                Event(R.id.sign_in, EventType.CLICK, this::loginWithEmailAndPassword))
    }

    val user = MutableLiveData(User("", "", UserParameters("", "")))

    private val googleMutex = Mutex(true)
    private fun loginWithGoogle() {
        viewModelScope.launch {
            val intent = firebaseAuthRepository.getGoogleLoginIntent()
            intent?.let {
                navigator.startIntent(it, 0)
                googleMutex.lock()
                firebaseAuthRepository.signInWithGoogleAccount(it)
                        .flowOn(Dispatchers.IO)
                        .handleResource(onSuccess = {
                            viewModelScope.launch(Dispatchers.IO + baseCoroutineExceptionHandler) {
                                userRepository.registerUser()
                                navigator.toRoot()
                            }
                        })
            } ?: run {
                throw Exception("intent not started, internal error")
            }
        }
    }

    private fun forgotPass() = navigator.navigate(LoginFragmentDirections.toForgotPassFragment())
    private fun createAcc() = navigator.navigate(LoginFragmentDirections.toCreateAccountFragment())

    private fun loginWithEmailAndPassword() =
            firebaseAuthRepository.loginWithCredentials(user = user.value!!)
                    .flowOn(Dispatchers.IO)
                    .handleResource(onSuccess = { navigator.toRoot() })

    fun handleGoogleSignInResult() = googleMutex.unlock()

    val passwordCriteria = PasswordErrorCriteria()
    val emailCriteria = EmailCriteria()
    val confirmButtonCriteria = ConfirmButtonErrorCriteria(arrayListOf(passwordCriteria, emailCriteria))
}
