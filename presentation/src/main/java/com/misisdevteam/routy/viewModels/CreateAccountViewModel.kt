package com.misisdevteam.routy.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.misisdevteam.routy.R
import com.misisdevteam.routy.data.User
import com.misisdevteam.routy.data.UserParameters
import com.misisdevteam.routy.data.enums.EventType
import com.misisdevteam.routy.repository.FirebaseAuthRepository
import com.misisdevteam.routy.repository.UserRepository
import com.misisdevteam.routy.utils.editTextUtils.ConfirmButtonErrorCriteria
import com.misisdevteam.routy.utils.editTextUtils.ConfirmPasswordErrorCriteria
import com.misisdevteam.routy.utils.editTextUtils.EmailCriteria
import com.misisdevteam.routy.utils.editTextUtils.PasswordErrorCriteria
import com.misisdevteam.routy.view.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class CreateAccountViewModel(
    private val firebaseAuthRepository: FirebaseAuthRepository,
    private val userRepository: UserRepository
) :
    BaseViewModel() {

    init {
        registerScreenEvents(Event(R.id.create_account_button, EventType.CLICK, this::createAccountByCredentials),
                Event(R.id.register_already, EventType.CLICK) { navigator.goBack(0) })
    }

    private fun createAccountByCredentials() = firebaseAuthRepository
                .createUserWithCredentials(User(login = "",
                        parameters = UserParameters(password = passwordText.value!!,
                                email = emailText.value!!)))
                .flowOn(Dispatchers.IO)
                .handleResource(onSuccess = {
                    viewModelScope.launch(baseCoroutineExceptionHandler) {
                        userRepository.registerUser()
                        navigator.toRoot()
                    }
                })


    val passwordText = MutableLiveData("")
    val confirmPasswordText = MutableLiveData("")
    val emailText = MutableLiveData("")

    val passwordCriteria = PasswordErrorCriteria()
    val confirmPasswordCriteria = ConfirmPasswordErrorCriteria(passwordText)
    val emailCriteria = EmailCriteria()
    val confirmButtonCriteria = ConfirmButtonErrorCriteria(arrayListOf(passwordCriteria, confirmPasswordCriteria, emailCriteria))
}
