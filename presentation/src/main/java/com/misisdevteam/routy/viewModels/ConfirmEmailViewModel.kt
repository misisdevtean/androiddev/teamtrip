package com.misisdevteam.routy.viewModels

import androidx.lifecycle.viewModelScope
import com.misisdevteam.routy.repository.FirebaseAuthRepository
import com.misisdevteam.routy.view.base.BaseViewModel
import kotlinx.coroutines.launch

class ConfirmEmailViewModel(
        private var firebaseAuthRepository: FirebaseAuthRepository
) :
        BaseViewModel() {
    init {
        viewModelScope.launch {
            firebaseAuthRepository.verifyEmail().also {
                navigator.toRoot()
            }
        }
    }
}
