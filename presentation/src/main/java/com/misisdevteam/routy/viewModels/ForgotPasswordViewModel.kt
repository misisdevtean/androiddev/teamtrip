package com.misisdevteam.routy.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.misisdevteam.routy.repository.FirebaseAuthRepository
import com.misisdevteam.routy.utils.editTextUtils.ConfirmButtonErrorCriteria
import com.misisdevteam.routy.utils.editTextUtils.EmailCriteria
import com.misisdevteam.routy.view.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class ForgotPasswordViewModel(
    private var firebaseAuthRepository: FirebaseAuthRepository
) : BaseViewModel() {
    val forgotPassEmail = MutableLiveData<String>()

    fun restorePassword() = viewModelScope.launch {
/*        flow {
            emit(Resource.Loading)
            forgotPassEmail.value?.let { firebaseAuthRepository.resetPassword(it) }
            emit(Resource.Success(null))
        }.flowOn(Dispatchers.IO).catch { throwable ->
            throwable.printStackTrace()
            emit(Resource.Error(exception = Exception(throwable)))
        }.handleResource(onSuccess = { navigator.toRoot() })*/
        forgotPassEmail.value?.let {
            firebaseAuthRepository.resetPassword(it)
                    .flowOn(Dispatchers.IO)
                    .handleResource(onSuccess = { navigator.toRoot() })
        }
    }

    val emailCriteria = EmailCriteria()
    val confirmButtonCriteria = ConfirmButtonErrorCriteria(arrayListOf(emailCriteria))
}
