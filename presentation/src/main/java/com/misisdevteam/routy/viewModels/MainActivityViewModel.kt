package com.misisdevteam.routy.viewModels

import androidx.lifecycle.MutableLiveData
import com.misisdevteam.routy.view.base.BaseViewModel

class MainActivityViewModel: BaseViewModel() {

    val isBottomViewEnabled = MutableLiveData<Boolean>(true)
}
