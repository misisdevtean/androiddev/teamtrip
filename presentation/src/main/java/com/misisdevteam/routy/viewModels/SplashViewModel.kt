package com.misisdevteam.routy.viewModels

import com.misisdevteam.routy.view.base.BaseViewModel
import com.misisdevteam.routy.view.splash.SplashFragmentDirections

class SplashViewModel :
    BaseViewModel(){
    fun toMainFragment() = navigator.navigate(SplashFragmentDirections.toMainFragment())
}
