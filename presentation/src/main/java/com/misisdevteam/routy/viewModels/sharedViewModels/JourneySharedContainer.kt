package com.misisdevteam.routy.viewModels.sharedViewModels

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import androidx.annotation.PluralsRes
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.misisdevteam.routy.R
import com.misisdevteam.routy.data.Journey
import com.misisdevteam.routy.utils.extensions.toLocalDate
import com.misisdevteam.routy.utils.extensions.toTimestamp
import com.misisdevteam.routy.view.base.BaseViewModel
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.Period
import org.threeten.bp.ZoneOffset


class JourneySharedContainer: BaseViewModel() {
    val startDate = MutableLiveData<LocalDate>()
    val endDate = MutableLiveData<LocalDate>()
    val name = MutableLiveData<String>()
    val formattedDates = ObservableField("")

    fun getJourney() = Journey(start_date = startDate.value?.toTimestamp(),
            end_date = endDate.value?.toTimestamp(),
            name = name.value)

    //private val participantsIcons = arrayListOf(Bitmap())
    private val mColors = arrayListOf(Color.BLACK, Color.BLUE, Color.CYAN, Color.GRAY, Color.GREEN)
    val usersStackAdapter = object : BaseAdapter() {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val colorLayoutParams = LinearLayout.LayoutParams(
                    32, 32)
            val colorLayout = LinearLayout(parent?.context)
            colorLayout.setBackgroundColor(mColors.get(position))
            colorLayout.layoutParams = colorLayoutParams

            return colorLayout
        }

        override fun getItem(position: Int): Any = mColors[position]

        override fun getItemId(position: Int): Long = position.toLong()

        override fun getCount(): Int {
            return getJourney().participants?.size ?: kotlin.run { 0 }
        }
    }


    fun navigateToSettings() {
        navigator.navigate(R.id.gotoJourneySettingsFragment)
    }


    data class RemainingTime(var quantity: Int, @PluralsRes var timeUnitPluralId: Int)

    val remainingTimeToTravel: MutableLiveData<RemainingTime>
        get() {
            getJourney().start_date?.let {
                val period = Period.between(LocalDate.now(),
                        Instant.ofEpochMilli(it).atOffset(ZoneOffset.UTC).toLocalDate()).plusDays(1)
                return when {
                    period.years != 0 -> {
                        MutableLiveData(RemainingTime(period.years, R.plurals.yearsPlural))
                    }
                    period.months != 0 -> {
                        MutableLiveData(RemainingTime(period.months, R.plurals.monthsPlural))
                    }
                    else -> {
                        MutableLiveData(RemainingTime(period.days, R.plurals.daysPlural))
                    }
                }
            }
            return MutableLiveData(RemainingTime(5, R.plurals.unknownPlural))
        }

    companion object{
        fun fromJourney(journey: Journey) : JourneySharedContainer = JourneySharedContainer().apply {
            startDate.value = journey.start_date?.toLocalDate()
            endDate.value = journey.end_date?.toLocalDate()
            name.value = journey.name
        }
    }
}