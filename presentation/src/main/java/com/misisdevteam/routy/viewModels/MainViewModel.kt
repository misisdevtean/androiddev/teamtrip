package com.misisdevteam.routy.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.google.firebase.auth.FirebaseUser
import com.misisdevteam.routy.R
import com.misisdevteam.routy.data.enums.EventType
import com.misisdevteam.routy.repository.FirebaseAuthRepository
import com.misisdevteam.routy.repository.JourneyRepository
import com.misisdevteam.routy.view.base.BaseViewModel
import com.misisdevteam.routy.view.blocks.mainNavBlock.mainPage.MainFragmentDirections

class MainViewModel(
        private val authRepository: FirebaseAuthRepository,
        private val journeyRepository: JourneyRepository
) :
    BaseViewModel() {
    init {
        registerScreenEvents(Event(R.id.login_or_register_button, EventType.CLICK, this::loginRegister),
                Event(R.id.create_journey_button, EventType.CLICK, this::createJourney))
        if (authRepository.getCurUser() != null) authRepository.logout()
    }
    var curUser = MutableLiveData<FirebaseUser?>(null)
    private val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(10)
            .build()

    //val pagedList : PagedList<JourneySharedContainer> = PagedList.Builder<>()//PagedList.Builder<JourneySharedContainer>(journeyRepository, config) .build()
//    val pagedList : PagedList<JourneySharedContainer> =
//            PagedList.Builder<JourneySharedContainer>(journeyRepository,config).

    fun refreshUser() {
        curUser.value = getUser()
    }

    private fun createJourney() {
        navigator.navigate(MainFragmentDirections.actionMainFragmentToCreateJourneyFragment())
    }

    private fun loginRegister() {
        navigator.navigate(MainFragmentDirections.toLoginFragment())
    }

    private fun getUser() = authRepository.getCurUser()
}
