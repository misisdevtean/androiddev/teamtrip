package com.misisdevteam.routy

import android.os.Bundle

interface NavigationResult {
    fun onNavigationResult(result: Bundle)
}