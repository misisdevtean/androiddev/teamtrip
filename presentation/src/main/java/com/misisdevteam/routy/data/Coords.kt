package com.misisdevteam.routy.data

data class Coords(
        var lat: Double?,
        var lng: Double?
)
