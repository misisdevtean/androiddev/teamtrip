package com.misisdevteam.routy.data

data class PlaceDetails(
        var address: String?,
        var coords: Coords?,
        var datasource: String?,
        var external_id: String?,
        var name: String?,
        var rating: Double?,
        var types: List<String>?
)