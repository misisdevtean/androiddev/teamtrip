package com.misisdevteam.routy.data

import com.google.gson.annotations.Expose

data class Journey(
        @Expose
        var description: String? = null,
        @Expose
        var end_date: Long? = null,
        @Expose(serialize = false, deserialize = true)
        var id: String? = null,
        @Expose
        var name: String? = null,
        @Expose
        var nested_journeys: List<Any>? = null,
        @Expose
        var participants: List<String>? = null,
        @Expose
        var place_details: PlaceDetails? = null,
        @Expose
        var start_date: Long? = null,
        @Expose
        var type: String? = null
)