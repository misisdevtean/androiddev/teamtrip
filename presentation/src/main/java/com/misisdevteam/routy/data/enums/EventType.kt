package com.misisdevteam.routy.data.enums

enum class EventType {
    CLICK,
    CHECKBOX_CHECKED,
    CHECKBOX_UNCHECKED
}