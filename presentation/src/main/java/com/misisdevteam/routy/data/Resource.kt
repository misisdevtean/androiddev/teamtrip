package com.misisdevteam.routy.data

import org.mockito.internal.matchers.Not

sealed class Resource<out T : Any> {
    object Ready : Resource<Nothing>()
    object Loading : Resource<Nothing>()
    open class Success<out T : Any>( val data: T?) : Resource<T>()
    data class Error(val exception: Exception) : Resource<Nothing>()
    object Finished : Resource<Nothing>()
}