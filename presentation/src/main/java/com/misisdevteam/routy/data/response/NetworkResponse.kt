package com.misisdevteam.routy.data.response


data class NetworkResponse<PAYLOAD>(
    val payload: PAYLOAD?,
    val request_id: String,
    val status: String,
    val errorCode : String? = null,
    val errorMsg: String? = null
)
