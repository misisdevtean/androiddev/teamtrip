package com.misisdevteam.routy.data

sealed class ScreenState {
    object Loading : ScreenState()
    class Error(val e : Exception?/*, val viewId: Int*/) : ScreenState()
}