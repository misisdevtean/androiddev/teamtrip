package com.misisdevteam.routy.data

data class UserJourneys(
    var payload: List<Journey>,
    var request_id: String,
    var status: String
) {
    constructor() : this(listOf(), "", "")
}
