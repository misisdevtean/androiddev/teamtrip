package com.misisdevteam.routy.data

import com.misisdevteam.routy.repository.FirebaseAuthRepository
import kotlinx.coroutines.*

class TokenController (
        private val firebaseAuthRepository: FirebaseAuthRepository) {
    var key: Deferred<String> = CoroutineScope(Dispatchers.IO).async(start = CoroutineStart.LAZY) {
        //firebaseAuthRepository.getToken(firebaseAuthRepository.getCurUser())?.let { it }
        firebaseAuthRepository.getToken(firebaseAuthRepository.getCurUser())?.let { it }
                ?: kotlin.run { "error" }
    }

/*    init {
        fetchToken()
    }*/

/*    fun fetchToken() = CoroutineScope(Dispatchers.IO).async {
        firebaseAuthRepository.getToken(firebaseAuthRepository.getCurUser())?.let { key = it }
        return@async key
    }*/
}