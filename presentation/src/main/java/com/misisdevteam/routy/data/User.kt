package com.misisdevteam.routy.data

import com.google.gson.annotations.Expose

data class User(
        @Expose
        var id: String? = null,
        @Expose
        var login: String? = null,
        @Expose
        var parameters: UserParameters? = null
)

data class UserParameters(
        @Expose
        var email: String? = null,
        @Expose(serialize = false, deserialize = false)
        var password: String? = null
)