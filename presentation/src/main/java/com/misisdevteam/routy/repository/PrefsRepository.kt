package com.misisdevteam.routy.repository

import android.content.SharedPreferences

class PrefsRepository(var preferences: SharedPreferences) {
    fun setLoginState(isLogin: Boolean): Boolean = preferences.edit().putBoolean("isLoginComplete", isLogin).commit()
    fun getLoginState(): Boolean = preferences.getBoolean("isLoginComplete", false)
}
