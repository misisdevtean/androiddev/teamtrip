package com.misisdevteam.routy.repository.api

import com.misisdevteam.routy.data.Journey
import com.misisdevteam.routy.data.response.NetworkResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface JourneyApi {

    @GET("v1.0/m/user/journeys")
    suspend fun getUserJourneys(): NetworkResponse<List<Journey>>

    @POST("/v1.0/m/user/journeys")
    suspend fun saveJourney(@Body journey: Journey) : NetworkResponse<Nothing>
}
