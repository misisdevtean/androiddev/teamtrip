package com.misisdevteam.routy.repository

import androidx.paging.PositionalDataSource
import com.misisdevteam.routy.data.Journey
import com.misisdevteam.routy.data.Resource
import com.misisdevteam.routy.repository.api.JourneyApi
import com.misisdevteam.routy.utils.extensions.ifSuccess
import com.misisdevteam.routy.utils.extensions.toResource
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.flow
import retrofit2.Retrofit

class JourneyRepository(retrofit: Retrofit) : PositionalDataSource<Journey>() {
    private val api: JourneyApi = retrofit.create(JourneyApi::class.java)
    private val scope = CoroutineScope(Dispatchers.IO)
    private var allJourneys = scope.async(start = CoroutineStart.LAZY) { api.getUserJourneys().toResource() }

    fun saveJourney(journey: Journey) = flow {
        emit(Resource.Loading)
        try {
            emit(api.saveJourney(journey).toResource())
        } catch (e: Exception) {
            emit(Resource.Error(e))
        }
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<Journey>) {
        scope.launch {
            val res = allJourneys.await()
            res.ifSuccess {
                if (it != null) {
                    val startIndex = if (params.startPosition > it.size - 1) it.size - 1 else params.startPosition
                    val endIndex = if (params.startPosition + params.loadSize - 1 > it.size - 1)
                        it.size - 1 else params.startPosition + params.loadSize - 1
                    callback.onResult(it.subList(startIndex, endIndex))
                }
            } ?: kotlin.run { callback.onResult(emptyList()) }
            //val startIndex = if (params.startPosition < allJourneys.await().data!!.size)
            //.subList(params.startPosition, params.startPosition + params.loadSize)?.let { callback.onResult(it) }
        }
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Journey>) {
        scope.launch {
            val res = allJourneys.await()
            res.ifSuccess {
                if (it != null) {
                    val startIndex = if (params.requestedStartPosition > it.size - 1) it.size - 1 else params.requestedStartPosition
                    val endIndex = if (params.requestedLoadSize + params.requestedLoadSize - 1 > it.size - 1)
                        it.size - 1 else params.requestedStartPosition + params.requestedLoadSize - 1
                    callback.onResult(it.subList(startIndex, endIndex), 0)
                }
            }
        }
    }
}
