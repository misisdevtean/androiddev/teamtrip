package com.misisdevteam.routy.repository.api

import com.misisdevteam.routy.data.User
import com.misisdevteam.routy.data.response.NetworkResponse
import okhttp3.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface UserApi {

    @POST("v1.0/m/user/register")
    suspend fun sendUser(): NetworkResponse<User>

    @GET("/v1.0/m/user/check-auth")
    suspend fun checkAuth(): Response
}
