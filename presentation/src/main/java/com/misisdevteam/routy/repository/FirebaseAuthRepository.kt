package com.misisdevteam.routy.repository

import android.content.Context
import android.content.Intent
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.misisdevteam.routy.data.Resource
import com.misisdevteam.routy.data.User
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import timber.log.Timber

class FirebaseAuthRepository(
        var auth: FirebaseAuth,
        val context: Context,
        val client: String
) {
    fun getCurUser() = auth.currentUser

    fun createUserWithCredentials(user: User) = flow {
        try {
            emit(Resource.Loading)
            Timber.i("create user: create account with credentials started")
            val createAccountTask = user.parameters?.email?.let { email ->
                user.parameters?.password?.let { pass -> auth.createUserWithEmailAndPassword(email, pass).await() }
            }
            Timber.i("create user : create account with credentials finished")
            emit(Resource.Success(createAccountTask?.user!!))
        } catch (e: Exception) {
            emit(Resource.Error(e))
        }
    }

    fun loginWithCredentials(user: User) = flow {
        try {
            emit(Resource.Loading)
            Timber.i("signIn user: sign in with credentials started")
            var signInTask = user.parameters?.email?.let { email ->
                user.parameters?.password?.let { pass ->
                    auth.signInWithEmailAndPassword(email, pass).await()
                }
            }
            Timber.i("signIn user: sign in with credentials finished")
            emit(Resource.Success(signInTask?.user!!))
        } catch (e: Exception) {
            emit(Resource.Error(e))
        }
    }

    fun getGoogleLoginIntent(): Intent? {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(client)
                .requestEmail()
                .build()
        return GoogleSignIn.getClient(context, gso).signInIntent
    }

    suspend fun signInWithGoogleAccount(intent: Intent) = flow {
        emit(Resource.Loading)
        try { // Google Sign In was successful, authenticate with Firebase
            val account: GoogleSignInAccount?
            account = try {
                val task = GoogleSignIn.getSignedInAccountFromIntent(intent)
                task.getResult(ApiException::class.java)
            } catch (e: ApiException) {
                Timber.e(e, "Getting google account from intent failed, trying get last signed account")
                GoogleSignIn.getLastSignedInAccount(context)
            }
            Timber.d("firebaseLoginWithGoogle: firebaseAuthWithGoogle:%s", account?.id)
            val credential = GoogleAuthProvider.getCredential(account?.idToken, null)

            auth.signInWithCredential(credential).await().user?.let { emit(Resource.Success(it)) }
                    ?: run {
                        emit(Resource.Error(Exception("Google account is empty")))
                    }
        } catch (e: ApiException) { // Google Sign In failed
            Timber.e(e, "Google sign in failed!!!")
            emit(Resource.Error(e))
        }
    }

    fun logout() = auth.signOut()

    fun resetPassword(email: String) = flow {
        emit(Resource.Loading)
        try {
            emit(Resource.Success(auth.sendPasswordResetEmail(email).await()))
        } catch (e: Exception) {
            emit(Resource.Error(e))
        }
    }

    suspend fun verifyEmail() {
        val user = getCurUser()!!
        user.sendEmailVerification()
        while (!(user.isEmailVerified)) {
            delay(200)
        }
    }

    suspend fun getToken(user: FirebaseUser?) = user?.getIdToken(true)?.await()?.token
}
