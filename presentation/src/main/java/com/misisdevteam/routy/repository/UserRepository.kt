package com.misisdevteam.routy.repository

import com.google.gson.Gson
import com.misisdevteam.routy.repository.api.UserApi
import retrofit2.Retrofit

class UserRepository (private val retrofit: Retrofit, private val gson: Gson) {
    suspend fun registerUser() = retrofit.create(UserApi::class.java)
        .sendUser()
    suspend fun checkAuth() = retrofit.create(UserApi::class.java)
        .checkAuth()
}
