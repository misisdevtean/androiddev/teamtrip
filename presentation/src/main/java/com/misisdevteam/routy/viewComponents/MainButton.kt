package com.misisdevteam.routy.viewComponents

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import com.misisdevteam.routy.R


class MainButton : ConstraintLayout {

    private var leftDrawable: Drawable? = null
    private var mainText: String? = null
    private var textColor: Int? = null

    private var imageView: ImageView? = null
    private var textView: TextView? = null

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        obtainResources(attrs)
        initView()
    }

    constructor(context: Context?) : super(context) {
        initView()
    }

    constructor(context: Context?, attrs: AttributeSet?, style: Int) : super(context, attrs, style) {
        obtainResources(attrs)
        initView()
    }


    private fun obtainResources(attrs: AttributeSet?) {
        val attrsArray = context.obtainStyledAttributes(attrs, R.styleable.MainButton)
        try {
            val drawId = attrsArray.getResourceId(R.styleable.MainButton_iconDrawable, -1)
            leftDrawable = AppCompatResources.getDrawable(context, drawId)
            mainText = attrsArray.getString(R.styleable.MainButton_android_text)
            textColor = attrsArray.getColor(R.styleable.MainButton_android_textColor, Color.BLACK)
        } finally {
            attrsArray.recycle()
        }
    }

    private fun initView() {
        val view = LayoutInflater.from(context).inflate(R.layout.teamtrip_button, this, false)
        addView(view)
        textView = findViewById(R.id.textView2)
        imageView = findViewById(R.id.imageView)

        imageView?.apply {
            setImageDrawable(leftDrawable)
        }
        textView?.apply {
            text = mainText
            this.setTextColor(textColor?.let { ColorStateList.valueOf(it) })
        }
    }

    fun convertToDp(dpValue : Int) : Int{
        val d = context.resources.displayMetrics.density
        return (dpValue * d).toInt()
    }
}
