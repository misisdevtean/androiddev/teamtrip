package com.misisdevteam.routy.utils

object Constants {
    const val BACKEND_URL = "https://routybackend-esgraxpmvq-ew.a.run.app"
    const val PASSWORDS_NOT_MATCH = "Пароли не совпадают"
    const val SHORT_PASSWORD = "Пароль слишком короткий"
    const val WRONG_EMAIL = "Введите правильный email"
}
