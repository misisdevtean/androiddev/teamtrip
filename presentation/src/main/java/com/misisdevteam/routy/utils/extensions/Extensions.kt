package com.misisdevteam.routy.utils.extensions

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import com.misisdevteam.routy.data.Journey
import com.misisdevteam.routy.data.Resource
import com.misisdevteam.routy.data.response.NetworkResponse
import org.threeten.bp.*
import org.threeten.bp.temporal.WeekFields
import timber.log.Timber
import java.lang.Exception
import java.lang.IllegalStateException
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


fun daysOfWeekFromLocale(): Array<DayOfWeek> {
    val firstDayOfWeek = WeekFields.of(Locale.FRANCE).firstDayOfWeek
    var daysOfWeek = DayOfWeek.values()
    // Order `daysOfWeek` array so that firstDayOfWeek is at index 0.
    if (firstDayOfWeek != DayOfWeek.MONDAY) {
        val rhs = daysOfWeek.sliceArray(firstDayOfWeek.ordinal..daysOfWeek.indices.last)
        val lhs = daysOfWeek.sliceArray(0 until firstDayOfWeek.ordinal)
        daysOfWeek = rhs + lhs
    }
    return daysOfWeek
}

fun GradientDrawable.setCornerRadius(
    topLeft: Float = 0F,
    topRight: Float = 0F,
    bottomRight: Float = 0F,
    bottomLeft: Float = 0F
) {
    cornerRadii = arrayOf(
        topLeft, topLeft,
        topRight, topRight,
        bottomRight, bottomRight,
        bottomLeft, bottomLeft
    ).toFloatArray()
}

fun Context.getDrawableCompat(@DrawableRes drawable: Int) = ContextCompat.getDrawable(this, drawable)

fun Context.getColorCompat(@ColorRes color: Int) = ContextCompat.getColor(this, color)

fun TextView.setTextColorRes(@ColorRes color: Int) = setTextColor(context.getColorCompat(color))

fun View.makeVisible() {
    visibility = View.VISIBLE
}

fun View.makeInVisible() {
    visibility = View.INVISIBLE
}

fun View.makeGone() {
    visibility = View.GONE
}

fun View.setMargins(top: Int, bottom: Int, left: Int, right: Int) {
    val d = context.resources.displayMetrics.density.toInt()
    Timber.i("screen density =  $d")
    (layoutParams as ViewGroup.MarginLayoutParams).apply {
        topMargin = top * d
        bottomMargin = bottom * d
        leftMargin = left * d
        rightMargin = right * d
    }
}

fun <T> MutableLiveData<T>.notifyObserver() {
    this.value = this.value
}

fun String.isMailCorrect(): Boolean {
    val regex = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$"
    val pattern: Pattern = Pattern.compile(regex)
    val matcher: Matcher = pattern.matcher(this)
    return matcher.matches()
}
fun String.isUsernameValid(): Boolean {
    return this.isNotEmpty()
}

inline fun <reified T : Observable> T.addOnPropertyChanged(crossinline callback: (T) -> Unit) =
    object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, i: Int) =
            callback(sender as T)
    }.also { addOnPropertyChangedCallback(it) }

fun View.setViewColor(@ColorRes color : Int) {
    when (val background = background) {
        is ShapeDrawable -> {
            background.paint.color = ContextCompat.getColor(context, color)
        }
        is GradientDrawable -> {
            background.setColor(ContextCompat.getColor(context, color))
        }
        is ColorDrawable -> {
            background.color = ContextCompat.getColor(context, color)
        }
    }
}

fun journey(initJourney: Journey? = null, init: Journey.() -> Unit): Journey {
    val journey = initJourney?.let { it } ?: run { Journey() }
    journey.init()
    return journey
}


fun LocalDate.toTimestamp(): Long = this.atStartOfDay(ZoneId.systemDefault()).toEpochSecond()*1000
fun Long.toLocalDate(): LocalDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(this),
        ZoneId.systemDefault()).toLocalDate()

fun Bitmap.getRoundedCornerBitmap(pixels: Int) {
    val output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(output)
    val color = -0xbdbdbe
    val paint = Paint()
    val rect = Rect(0, 0, width, height)
    val rectF = RectF(rect)
    val roundPx = pixels.toFloat()
    paint.isAntiAlias = true
    canvas.drawARGB(0, 0, 0, 0)
    paint.color = color
    canvas.drawRoundRect(rectF, roundPx, roundPx, paint)
    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
    canvas.drawBitmap(this, rect, rect, paint)
}


@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
fun Context.getResourcesByLocale(): Resources {
    val configuration = Configuration(resources.configuration)
    configuration.setLocale(Locale("ru"))
    return createConfigurationContext(configuration).resources
}

inline fun <T : Any> NetworkResponse<T>.toResource(): Resource<T> {
    if (this.status == "ok") {
        this.payload?.let { return Resource.Success(it) } ?: kotlin.run {
            return Resource.Success(null)
        }
    }
    if (this.status == "error") return Resource.Error(Exception(this.errorMsg))
    return Resource.Loading
}

fun <T : Any> Resource<T>.unpack() : T? {
    when (this) {
        is Resource.Success -> return this.data
        is Resource.Error -> throw this.exception
        else -> Timber.w("Resource not been computed yet!")
    }
    return null
}

inline fun <T : Any> Resource<T>.ifSuccess(lambda : (T?) -> Unit) {
    if (this is Resource.Success) lambda.invoke(this.data)
}

inline fun <T : Any> Resource<T>.ifFailure(lambda : (Exception) -> Unit) {
    if (this is Resource.Error) lambda.invoke(this.exception)
}