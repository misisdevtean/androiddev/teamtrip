package com.misisdevteam.routy.utils

import android.app.Activity
import android.content.res.ColorStateList
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.*
import androidx.annotation.PluralsRes
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.airbnb.lottie.LottieCompositionFactory
import com.airbnb.lottie.LottieDrawable
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.misisdevteam.routy.R
import com.misisdevteam.routy.utils.editTextUtils.BaseButtonErrorCriteria
import com.misisdevteam.routy.utils.editTextUtils.BaseEditTextErrorCriteria
import com.misisdevteam.routy.utils.editTextUtils.BaseErrorCriteria
import com.misisdevteam.routy.utils.extensions.addOnPropertyChanged
import com.misisdevteam.routy.utils.extensions.getResourcesByLocale
import timber.log.Timber


object BindingAdapters {

    @BindingAdapter("bind:errorBackgroundCriteria", "bind:errorTextHelper", requireAll = false)
    @JvmStatic
    fun setErrorBackground(
            criteriaView: View,
            criteria: BaseErrorCriteria,
            errorTextHelper: String? = null
    ) {
        when (criteriaView) {
            is EditText -> setEditTextCriteria(criteriaView, criteria as BaseEditTextErrorCriteria, errorTextHelper)
            is Button -> setButtonCriteria(criteriaView, criteria as BaseButtonErrorCriteria)
        }
    }

    @BindingAdapter("bind:drawable")
    @JvmStatic
    fun setDrawable(view: View, drawable: Drawable) {
    }

    private fun setEditTextCriteria(
        editText: View,
        criteria: BaseEditTextErrorCriteria,
        errorTextHelper: String?
    ) {
        (editText as EditText).addTextChangedListener(object : TextWatcher {
            var enabled = false
            override fun afterTextChanged(s: Editable?) {
                if (!enabled && criteria.applyCriteria(s.toString())) {
                    editText.setBackgroundResource(R.drawable.edit_text_error_shape)
                    ((editText as TextInputEditText).parent.parent as TextInputLayout).apply {
                        errorTextHelper?.let {
                            this.helperText = errorTextHelper
                            isHelperTextEnabled = true
                        }
                        val errorColor = ColorStateList.valueOf(resources.getColor(R.color.red, null))
                        counterTextColor = errorColor
                        setHelperTextColor(errorColor)
                        hintTextColor = errorColor
                    }
                    enabled = true
                }
                if (enabled && !criteria.applyCriteria(s.toString())) {
                    editText.setBackgroundResource(R.drawable.blurred_black_shape_8dp_orange_stroke)
                    ((editText as TextInputEditText).parent.parent as TextInputLayout).apply {
                        val normalOrangeColor = ColorStateList.valueOf(resources.getColor(R.color.orange, null))
                        val normalGrayColor = ColorStateList.valueOf(resources.getColor(R.color.grey_50, null))
                        errorTextHelper?.let { isHelperTextEnabled = false }
                        counterTextColor = normalGrayColor
                        setHelperTextColor(normalGrayColor)
                        hintTextColor = normalOrangeColor
                    }
                    enabled = false
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    private fun setButtonCriteria(button: View, criteria: BaseButtonErrorCriteria) {
        button.isEnabled = false
        criteria.isError.addOnPropertyChanged {
            button.isEnabled = !it.get()
        }
    }

    @BindingAdapter("bind:lottieAnimation")
    @JvmStatic
    fun setLottieAnimation(view: View, drawable: Drawable) {
        val anim = LottieDrawable()
        LottieCompositionFactory.fromRawRes(view.context, R.raw.test_animation).addListener {
            anim.composition = it
            anim.bounds = Rect(0, 0, view.right, view.bottom)
            view.background = anim
            anim.playAnimation()
        }
    }

    @BindingAdapter("bind:underlineText")
    @JvmStatic
    fun setUnderline(view: TextView, isUnderline: Boolean) {
        val content = SpannableString(view.text)
        content.setSpan(UnderlineSpan(), 0, view.text.length, 0)
        view.text = content
    }

    @BindingAdapter("bind:bottomSheetOnClick")
    @JvmStatic
    fun callBottomSheetFragment(view: View, tag: String) {
        (view.context as Activity)
        /* val selectTaxesFragment = SelectTaxesFragment()
         selectTaxesFragment.arguments = Bundle().also {
             it.putStringArrayList("taxes", formattedTaxesList)
         }
         selectTaxesFragment.setTargetFragment(this, Constants.FRAGMENT_CODE)
         fragmentManager?.let { fm -> selectTaxesFragment.show(fm, "SelectTaxeFragment") }*/
    }

    @BindingAdapter("bind:adapter")
    @JvmStatic
    fun setAdapter(view: View, adapter: Any) {
        /*if (!((view is RecyclerView && adapter is RecyclerView.Adapter<*>) || (view is StackView && adapter is BaseAdapter))){
            Timber.e("type error with $view and $adapter")
            return
        }*/

        when (view) {
            is StackView -> {
                if (adapter !is BaseAdapter) {
                    Timber.e("Wrong type: $adapter with $view")
                } else
                    view.adapter = adapter
            }
            is RecyclerView -> {
                if (adapter !is RecyclerView.Adapter<*>) {
                    Timber.e("Wrong type: $adapter with $view")
                } else
                    view.adapter = adapter
            }
            is ViewPager2 -> {
                if (adapter !is RecyclerView.Adapter<*>) {
                    Timber.e("Wrong type: $adapter with $view")
                } else
                    view.adapter = adapter
            }
        }
    }

    @BindingAdapter("bind:pluralsId", "bind:quantity", requireAll = true)
    @JvmStatic
    fun setPluralText(view: TextView, @PluralsRes pluralId: Int, quantity: Int) {
        val context = view.context
        view.text = context.getResourcesByLocale().getQuantityString(pluralId, quantity, quantity)
    }

    @BindingAdapter("bind:test")
    fun a(view : EditText, enabled : Boolean){
        //view.window getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        if(enabled) view.setOnTouchListener { v, event -> true }
    }
}
