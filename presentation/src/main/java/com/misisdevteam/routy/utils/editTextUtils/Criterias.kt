package com.misisdevteam.routy.utils.editTextUtils

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import com.misisdevteam.routy.utils.extensions.addOnPropertyChanged
import com.misisdevteam.routy.utils.extensions.isMailCorrect

interface BaseErrorCriteria {
    var isError: ObservableBoolean
}

abstract class BaseEditTextErrorCriteria : BaseErrorCriteria {
    override var isError = ObservableBoolean(true)
    fun applyCriteria(text: String) = criteria(text).also {
        isError.apply {
            this.set(it)
            this.notifyChange()
        }
    }

    protected abstract fun criteria(text: String): Boolean
}

abstract class BaseButtonErrorCriteria : BaseErrorCriteria {
    override var isError = ObservableBoolean(false)
}

class PasswordErrorCriteria : BaseEditTextErrorCriteria() {
    override fun criteria(text: String) = text.length < 8
}

class ConfirmPasswordErrorCriteria(private val pass: LiveData<String>) : BaseEditTextErrorCriteria() {
    override fun criteria(text: String) = text != pass.value
}

class NotNullCriteria : BaseEditTextErrorCriteria() {
    override fun criteria(text: String): Boolean = text.isBlank()

}
class EmailCriteria : BaseEditTextErrorCriteria() {
    override fun criteria(text: String) = !text.isMailCorrect()
}

class ConfirmButtonErrorCriteria(args: ArrayList<BaseErrorCriteria>) : BaseButtonErrorCriteria() {
    private val controls = ArrayList<Boolean>()

    init {
        repeat(args.size) {
            controls.add(true)
        }
        for ((i, criteria) in args.withIndex()) {
            criteria.isError.addOnPropertyChanged {
                controls[i] = it.get()
                if (!controls.contains(true))
                    this.isError.set(false)
                else this.isError.set(true)
            }
        }
    }
}
