package com.misisdevteam.routy.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.misisdevteam.routy.R
import com.misisdevteam.routy.data.Journey
import com.misisdevteam.routy.databinding.JourneyRecyclerViewItemBindingImpl
import com.misisdevteam.routy.viewModels.sharedViewModels.JourneySharedContainer


class JourneyRecyclerAdapter(private val journeys: List<Journey>) :
        PagedListAdapter<JourneySharedContainer, JourneyRecyclerAdapter.JourneyVH>(JourneyDiffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JourneyVH {
        val dataBinding: JourneyRecyclerViewItemBindingImpl =
                DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                        R.layout.journey_recycler_view_item, parent, false)
        return JourneyVH(dataBinding)
    }

    override fun onBindViewHolder(holder: JourneyVH, position: Int) {
        holder.itemBiding.journey = JourneySharedContainer.fromJourney(journeys[position])
        val config = PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setPageSize(5)
                .build()
    }


    inner class JourneyVH(val itemBiding: JourneyRecyclerViewItemBindingImpl)
        : RecyclerView.ViewHolder(itemBiding.root) {
    }

    object JourneyDiffUtil : DiffUtil.ItemCallback<JourneySharedContainer>() {
        override fun areItemsTheSame(oldItem: JourneySharedContainer, newItem: JourneySharedContainer): Boolean {
            return oldItem.getJourney().id  == newItem.getJourney().id
        }

        override fun areContentsTheSame(oldItem: JourneySharedContainer, newItem: JourneySharedContainer): Boolean {
            return newItem.getJourney().name == oldItem.getJourney().name &&
                    newItem.getJourney().description == oldItem.getJourney().description &&
                    newItem.getJourney().type == oldItem.getJourney().type &&
                    newItem.getJourney().start_date == oldItem.getJourney().start_date &&
                    newItem.getJourney().end_date == oldItem.getJourney().end_date &&
                    newItem.getJourney().participants == oldItem.getJourney().participants &&
                    newItem.getJourney().nested_journeys == oldItem.getJourney().nested_journeys
        }
    }
}

