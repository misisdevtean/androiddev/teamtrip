package com.misisdevteam.routy

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.misisdevteam.routy.di.component.appComponent
import com.misisdevteam.routy.di.module.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber
import timber.log.Timber.DebugTree

open class TeamTripApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@TeamTripApp)
            modules(appComponent)
        }
        AndroidThreeTen.init(this) // for calendarView
        if (BuildConfig.DEBUG)
            Timber.plant(DebugTree())
    }

}
