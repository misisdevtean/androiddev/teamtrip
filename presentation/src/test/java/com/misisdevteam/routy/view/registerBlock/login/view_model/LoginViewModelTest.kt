package com.misisdevteam.routy.view.registerBlock.login.view_model

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun getUsername() {
    }

    @Test
    fun setUsername() {
    }

    @Test
    fun getPassword() {
    }

    @Test
    fun setPassword() {
    }

    @Test
    fun getEmail() {
    }

    @Test
    fun setEmail() {
    }

    @Test
    fun isReadyToLogin() {
    }

    @Test
    fun setReadyToLogin() {
    }

    @Test
    fun loginWithGoogle() {
    }

    @Test
    fun forgotPass() {
    }

    @Test
    fun createAcc() {
    }

    @Test
    fun loginWithEmailAndPassword() {
    }

    @Test
    fun `isLoginedAlready$app_debug`() {
    }

    @Test
    fun getFirebaseAuthRepository() {
    }

    @Test
    fun setFirebaseAuthRepository() {
    }
}
