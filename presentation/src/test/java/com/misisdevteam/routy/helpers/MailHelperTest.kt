package com.misisdevteam.routy.helpers

import com.misisdevteam.routy.utils.extensions.isMailCorrect
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class MailHelperTest {

    @Test
    fun isMailCorrect() {
        assertTrue("volodkov97@gmail.com".isMailCorrect())
        assertFalse("volodkov97@qwerty".isMailCorrect())
        assertFalse("volodkov97gmail.com".isMailCorrect())
        assertFalse("".isMailCorrect())
        assertFalse("volodkov97@gmail.com.1com".isMailCorrect())
        assertFalse("volodkov97@@gmail.com".isMailCorrect())
        assertFalse("volodkov97@@gmail.com1".isMailCorrect())
    }
}
