plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
    id("com.google.gms.google-services")
    id("androidx.navigation.safeargs.kotlin")
    id("org.jlleitschuh.gradle.ktlint")
    id("io.gitlab.arturbosch.detekt")
    id("com.google.firebase.appdistribution")
}
//apply { from("../jacoco.gradle") }

configure<com.android.build.gradle.AppExtension> {
    compileSdkVersion(29)
    defaultConfig {
        applicationId = "com.misisdevteam.routy"
        minSdkVersion(23)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "0.0.13"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    signingConfigs {
        named("debug").configure {
            storeFile = file("keystores")
            storePassword = "EzV24m29"
            keyAlias = "RoutyDebugKey"
            keyPassword = "EzV24m29"
        }
        register("release") {
            storeFile = file("keystores")
            storePassword = "EzV24m29"
            keyAlias = "RoutyDebugKey"
            keyPassword = "EzV24m29"
        }
    }

    dataBinding {
        isEnabled = true
    }

    lintOptions {
        isAbortOnError = false
    }

    buildTypes {
        defaultConfig {
            firebaseAppDistribution {
                /*releaseNotes = "Notes\n" + getCommitMessages()
                groups = "android-testers"*/
                serviceCredentialsFile = "rugged-future-273009-7beec831a665.json"
            }
        }
        getByName("release") {
            isMinifyEnabled = false
            setProguardFiles(listOf(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"))
            signingConfig = signingConfigs.getByName("release")
        }
        getByName("debug") {
            isTestCoverageEnabled = true
            isMinifyEnabled = false
            signingConfig = signingConfigs.getByName("debug")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildToolsVersion = "29.0.2"

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
            // freeCompilerArgs = listOf("-Xallow-result-return-type")
        }
    }
    lintOptions {
        isAbortOnError = false
    }
}

val implementation by configurations
val testImplementation by configurations
val androidTestImplementation by configurations
val api by configurations
val kapt by configurations

dependencies {

    implementation (project(":usecase"))
    implementation (project(":data"))
    implementation (project(":domain"))
    implementation (project(":framework"))


    val lifecycleVersion = "2.2.0"
    val archVersion = "2.1.0"
    val koinVersion = "2.1.5"

    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.3.72")
    implementation("androidx.appcompat:appcompat:1.1.0")
    implementation("androidx.core:core-ktx:1.2.0")
    implementation("androidx.constraintlayout:constraintlayout:2.0.0-beta6")
    implementation("androidx.fragment:fragment-ktx:1.2.4")
    implementation("com.google.android.material:material:1.1.0")
    implementation("androidx.annotation:annotation:1.1.0")
    // ViewModel
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion")
    // LiveData
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion")
    // Lifecycles only (without ViewModel or LiveData)
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:$lifecycleVersion")
    // Saved state module for ViewModel
    implementation("androidx.lifecycle:lifecycle-viewmodel-savedstate:${lifecycleVersion}")
    // Annotation processor
    kapt("androidx.lifecycle:lifecycle-compiler:$lifecycleVersion")
    // alternately - if using Java8, use the following instead of lifecycle-compiler
    implementation("androidx.lifecycle:lifecycle-common-java8:$lifecycleVersion")
    // optional - helpers for implementing LifecycleOwner in a Service
    implementation("androidx.lifecycle:lifecycle-service:$lifecycleVersion")
    // optional - ProcessLifecycleOwner provides a lifecycle for the whole application process
    implementation("androidx.lifecycle:lifecycle-process:$lifecycleVersion")
    // optional - ReactiveStreams support for LiveData
    implementation("androidx.lifecycle:lifecycle-reactivestreams-ktx:$lifecycleVersion")
    // optional - Test helpers for LiveData
    testImplementation("androidx.arch.core:core-testing:$archVersion")

    implementation("com.google.firebase:firebase-auth:19.3.1")
    implementation("com.google.firebase:firebase-storage:19.1.1")

    implementation("com.google.firebase:firebase-analytics:17.4.2")
    /**
     *  Koin
     */
    // Koin for Kotlin
    implementation ("org.koin:koin-core:$koinVersion")
    // Koin extended & experimental features
    implementation ("org.koin:koin-core-ext:$koinVersion")
    // Koin for Unit tests
    testImplementation ("org.koin:koin-test:$koinVersion")
    // Koin for Android
    implementation ("org.koin:koin-android:$koinVersion")
    // Koin Android Scope features
    implementation ("org.koin:koin-android-scope:$koinVersion")
    // Koin Android ViewModel features
    implementation ("org.koin:koin-android-viewmodel:$koinVersion")
    // Koin Android Experimental features
    implementation ("org.koin:koin-android-ext:$koinVersion")
    // Koin AndroidX Scope features
    implementation ("org.koin:koin-androidx-scope:$koinVersion")
    // Koin AndroidX ViewModel features
    implementation ("org.koin:koin-androidx-viewmodel:$koinVersion")
    // Koin AndroidX Fragment features
    implementation ("org.koin:koin-androidx-fragment:$koinVersion")
    // Koin AndroidX Experimental features
    implementation ("org.koin:koin-androidx-ext:$koinVersion")
    /* api("com.google.dagger:dagger:2.27")
     kapt("com.google.dagger:dagger-compiler:2.27")
     api("com.google.dagger:dagger-android:2.27")
     api("com.google.dagger:dagger-android-support:2.27")
     kapt("com.google.dagger:dagger-android-processor:2.27")
     implementation("com.google.dagger:dagger-producers:2.27")*/


    implementation("com.google.code.gson:gson:2.8.6")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7")
    implementation("androidx.security:security-crypto:1.0.0-rc01")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.7")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-play-services:1.0.0")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.3.72")
    /**
     * Coil
     */
    implementation("io.coil-kt:coil:0.11.0")

    implementation("com.firebaseui:firebase-ui-storage:4.3.1")
    implementation("com.google.firebase:firebase-core:17.4.2")
    implementation("com.google.firebase:firebase-database:19.3.0")
    implementation("com.google.android.gms:play-services-maps:17.0.0")
    implementation("com.google.android.gms:play-services-auth:18.0.0")
    /**
     *  android view pager 2
     */
    implementation("androidx.viewpager2:viewpager2:1.0.0")

    testImplementation("org.robolectric:robolectric:4.3.1")
    implementation("androidx.navigation:navigation-fragment-ktx:2.2.2")
    implementation("androidx.navigation:navigation-ui-ktx:2.2.2")

    implementation("androidx.ui:ui-tooling:0.1.0-dev11")
    implementation("androidx.ui:ui-layout:0.1.0-dev11")
    implementation("androidx.ui:ui-material:0.1.0-dev11")

    /**
     * testing
     */
    testImplementation("junit:junit:4.13")
    androidTestImplementation("androidx.test.ext:junit:1.1.1")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0")
    /**
     * mockito
     */
    implementation("org.mockito:mockito-core:3.3.0")
    /**
     * kaspresso
     */
    androidTestImplementation("com.kaspersky.android-components:kaspresso:1.0.1")
    /**
     * kakao
     */
    androidTestImplementation("com.agoda.kakao:kakao:2.2.0")
    /**
     * calendar view
     */
    implementation("com.github.kizitonwose:CalendarView:0.3.4")

    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")
    implementation("com.squareup.okhttp3:logging-interceptor:4.7.2")

    /**
     * paging
     */
    implementation("androidx.paging:paging-runtime:2.1.2")

    /**
     * Timber
     */
    implementation("com.jakewharton.timber:timber:4.7.1")

    /**
     * Lottie
     */
    implementation("com.airbnb.android:lottie:3.4.0")

    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.7.4")
}

ktlint {
    android.set(true)
    outputColorName.set("RED")
    ignoreFailures.set(true)
    enableExperimentalRules.set(true)
    reporters {
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.PLAIN)
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.CHECKSTYLE)
    }
}

detekt {
    debug = true
    buildUponDefaultConfig = true
    config = files(project.rootDir.resolve("reports/failfast.yml"))
    baseline = project.rootDir.resolve("reports/baseline.xml")
    ignoreFailures = true
    reports {
        xml {
            enabled = true
            destination = file("buildreports/detekt.xml")
        }
        html {
            enabled = true
            destination = file("buildreports/detekt.html")
        }
    }
}

val detektFormat by tasks.registering(io.gitlab.arturbosch.detekt.Detekt::class) {
    description = "Reformats whole code base."
    parallel = true
    disableDefaultRuleSets = true
    buildUponDefaultConfig = true
    autoCorrect = true
    setSource(files(projectDir))
    include("**/*.kt")
    include("**/*.kts")
    exclude("**/resources/**")
    exclude("**/build/**")
    config.setFrom(files(project.rootDir.resolve("reports/format.yml")))
    reports {
        xml { enabled = false }
        html { enabled = false }
    }
}

val detektAll by tasks.registering(io.gitlab.arturbosch.detekt.Detekt::class) {
    description = "Runs over whole code base without the starting overhead for each module."
    parallel = true
    buildUponDefaultConfig = true
    setSource(files(projectDir))
    config.setFrom(files(project.rootDir.resolve("reports/failfast.yml")))
    include("**/*.kt")
    include("**/*.kts")
    exclude("**/resources/**")
    exclude("**/build/**")
    baseline.set(project.rootDir.resolve("reports/baseline.xml"))
    reports {
        xml.enabled = false
        html.enabled = false
    }
}
